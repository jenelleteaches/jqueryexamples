
// variables
// javascript - loosely typed languages
var x = 5;
var y = "jenelle"

// console output
console.log(x)
console.log("jenelle is in class")

// string manipulation
// string interpolation
console.log("banana " +" "+ " apples")

// if statements
var a = 20;
if (a > 25) {
  console.log("apples")
}
else if (a <= 15) {
  console.log("bananas")
}
else {
  console.log("carrots")
}

// for loops (c style for loops )
// for (var i = 6 ; i < 10; i++) {
//   console.log("HELLO");
// }
//
// // while loops --> being able to run forever
// var b = 5
// while (b < 3) {
//   console.log("GOODYBE")
// }

// arrays + dictories --> collections
// collections
// -->  a single vairiable that
// stores many items inside it

var b = 4
var names = ["albert", "bob", "chris"]

console.log(names)
// 1. add to an collection
// 2. remove from teh collection
// 3. change an item in the collection

names[3] = "david"
console.log(names); // albert, bob, chris, david

names.push("eggplant")
console.log(names); // albert, bob, chris, david, eggplant

// remove
names.pop()
console.log(names); // albert, bob, chris, david

names.pop()
console.log(names); // albert, bob, chris

// Javsacript is treating your arrays
// like a LIFO data structure
// LIFO = last in , first output
// STACK  ---> stack of books

// FIFO = first in, first out
// QUEUE --> lining up at a cashier


//// ----- DICTIONARY
// -- unordered collection
// --- no positions
/// --- (keys, values)
// --- arrays
// --- ordered collections
//   0          1         2
// ["apple", "banana", "carrot"]


// simple dictionary
var student = {
  "name":"jenelle",
  "id":"23434",
  "dept":"wddm"
}

// example of how to output specific things from the dictionary
console.log(student["name"])
// Option 1 for getting the id
console.log(student["id"])
// Option 2 for getting the id --> your choice which option to choose
console.log(student.id)

// dictionary of airport locations
var airports = {
  "LHR":"London Heathrow",
  "YYZ":"Toronto International Airport",
  "YUL":"Montreal",
  "LAS":"Mccarren International (Las Vegas)"
}

console.log(airports);
// add something to your dictionary
airports["LAX"] = "Los angeles international"
console.log(airports);



// example of a dictionary with integers as keys
var responseCodes = {
  400:"bad request",
  404:"not found",
  200:"ok"
}


// example of a complicated dictionary
var weather ={
  "location": {
    "lat":34.555,
    "lng":128.55
    "name":"Sudbury"
  },
  "condition":"drizzle",
  "temp": 3,
  "forecast":[3,8,9,1,2,4],
  "datetime": {
    "day":"2018-04-01", // april 1, janur 4
    "time":"3:26 PM"
  }
}

// getting stuff out of a nested dictionary
// --------
// get the day
console.log(weather.datetime.day) // --- 2018-04-01
console.log(weather["datetime"]["day"])

// get the 4th thing out of the forecast array
console.log(weather.forecast[3])    // -- 1
console.log(weather["forecast"][3])
